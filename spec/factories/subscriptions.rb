FactoryBot.define do
  factory :subscription do
    name { "MyText" }
    publication
  end
end
