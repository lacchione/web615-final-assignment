require 'rails_helper'

RSpec.describe 'Subscriptions', type: :request do

  before(:each) do
    @user = FactoryBot.create(:admin) # Create the user
    @publication = FactoryBot.create(:publication)

    # Set up the basic premise of the test by making sure that you have to log in
    visit root_path
    expect(current_path).to eq(new_user_session_path)
    expect(current_path).to_not eq(root_path)

    # Within the form #new_user do the following
    # The reason I put this within a within block is so if there are 2 form fields
    # on the page called Email it will fill in only this one
    within('#new_user') do
      fill_in 'Email', with: @user.email
      fill_in 'Password', with: @user.password
      click_button 'Log in'
    end

    # Since we've logged in we should check if the application redirected us to the right path
    expect(current_path).to eq(root_path)
    expect(current_path).to_not eq(new_user_session_path)
    expect(page).to have_content('Signed in successfully.')
  end

  describe 'GET #index' do
    describe 'valid: ' do
      it 'should return a list of subscriptions' do
        @subscription = FactoryBot.create(:subscription)
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        expect(page).to have_content(@subscription.name)
        # save_and_open_page
      end
    end

    describe 'invalid: ' do
      # Since there's no real invalid version of this test we skip it
    end
  end


  describe 'GET #show' do
    describe 'valid: ' do
      it 'should return an subscription' do
        @subscription = FactoryBot.create(:subscription)
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        expect(page).to have_content(@subscription.name)

        click_link 'Show'
        expect(current_path).to eq(subscription_path(@subscription))

        expect(page).to have_content(@subscription.name)
        # save_and_open_page
      end
    end

    #  describe 'invalid: ' do
    #    it 'should not return an publication if one does not exist' do
    #      visit publication_path(99_999)
    #      expect(current_path).to eq(publications_path)
    #      expect(page).to have_content("The article you're looking for cannot be found")
    #      # save_and_open_page
    #    end
    #  end
  end


  describe 'GET #new' do
    describe 'valid: ' do
      it 'should create a new subscription with valid attributes' do
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        click_link 'New Subscription'
        expect(current_path).to eq(new_subscription_path)

        fill_in 'subscription_name', with: 'New_Subscription'
        select @publication.companyname, from: 'subscription[publication_id]'

        click_button 'Create Subscription'
        # save_and_open_page
        expect(page).to have_content('Subscription was successfully created.')
        expect(page).to have_content('New_Subscription')
      end
    end

    describe 'invalid: ' do
      it 'should not create a new subscription with invalid attributes' do
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        click_link 'New Subscription'
        expect(current_path).to eq(new_subscription_path)

        fill_in 'subscription_name', with: ''
        click_button 'Create Subscription'
        # save_and_open_page
        expect(page).to have_content("Name can't be blank")
      end
    end
  end

  describe 'GET #edit' do
    describe 'valid: ' do
      it 'should update an subscription with valid attributes' do
        @subscription = FactoryBot.create(:subscription)
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        expect(page).to have_content(@subscription.name)

        click_link 'Show'
        expect(current_path).to eq(subscription_path(@subscription))

        expect(page).to have_content(@subscription.name)

        click_link 'Edit'
        expect(current_path).to eq(edit_subscription_path(@subscription))

        fill_in 'subscription_name', with: 'Edited_Subscription_Name'
        click_button 'Update Subscription'

        expect(page).to have_content('Subscription was successfully updated.')
        expect(page).to have_content('Edited_Subscription_Name')
        expect(current_path).to eq(subscription_path(@subscription))
        # save_and_open_page
      end
    end

    describe 'invalid: ' do
      it 'should not update an subscription with invalid attributes' do
        @subscription = FactoryBot.create(:subscription)
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        expect(page).to have_content(@subscription.name)

        click_link 'Show'
        expect(current_path).to eq(subscription_path(@subscription))

        expect(page).to have_content(@subscription.name)

        click_link 'Edit'
        expect(current_path).to eq(edit_subscription_path(@subscription))

        fill_in 'subscription_name', with: ''
        click_button 'Update Subscription'

        expect(page).to have_content("Subscription name can't be blank")
        # save_and_open_page
      end
    end
  end

  describe 'DELETE #destroy' do
    describe 'valid: ' do
      it 'should destroy an subscription when destroy is clicked' do
        @subscription = FactoryBot.create(:subscription)
        click_link 'Subscriptions'
        expect(current_path).to eq(subscriptions_path)

        expect(page).to have_content(@subscription.name)
        click_link 'Destroy'

        save_page
        expect(current_path).to eq(subscriptions_path)
        expect(page).to have_content('Subscription was successfully destroyed.')
      end
    end
  end
end
