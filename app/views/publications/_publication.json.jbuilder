json.extract! publication, :id, :companyname, :created_at, :updated_at
json.url publication_url(publication, format: :json)
